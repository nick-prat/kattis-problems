#include<iostream>
#include<string>
#include<unordered_map>

using card_map = std::unordered_map<int, int>;

int get_value(char c) {
    switch(c) {
        case 'A':
            return 13;
        case 'K':
            return 12;
        case 'Q':
            return 11;
        case 'J':
            return 11;
        case 'T':
            return 10;
        default:
            return c - '0';
    }
}

int can_beat(card_map o, int val) {
    for (int i = val + 1; i <= 13; i++) {
        if (o[i] > 0) {
            return i;
        }
    }
    return 0;
}

int can_tie(card_map o, int val) {
    return o[val] > 0 ? val : 0;
}

int get_low(card_map p) {
    for (int i = 0; i <= 13; i++) {
        if(p[i] != 0) {
            return i;
        }
    }
    return 0;
}

card_map setup_map() {
    auto m = card_map{};
    for ( int i = 2; i <= 13; i++) {
        m[i] = 0;
    }
    return m;
}

void print_map(std::ostream& os, card_map m) {
    std::cout << '{';
    for(auto& [k,v] : m) {
        std::cout << k << ':' << v << ", ";
    }
    std::cout << "}\n";
}

int solve(int score, card_map& o, card_map& p) {
    for (int i = 13; i > 0; i--) {
        while(o[i] > 0) {
            o[i]--;

            auto res = can_beat(p, i);
            if (res != 2) {
                score = score + 2;
                p[res]--;
                continue;
            }

            auto plow = get_low(p);
            res = can_tie(p, i);
            if (res != 0) {
                p[res]--;
                auto tie_score = solve(score + 1, o, p);

                if (plow == res) {
                    return tie_score;
                }

                auto no = card_map{o};
                auto np = card_map{p};

                np[plow]--;
                auto loss_score = solve(score, no, np);
                return std::max(loss_score, tie_score);
            }

            plow--;
        }
    }

    return score;
}

int main(int argc, char** argv) {
    std::string line;
    std::getline(std::cin, line);
    auto N = std::stoi(line);

    for (int i = 0; i < N; i++) {
        std::string o, p;

        std::getline(std::cin, o);
        std::getline(std::cin, p);

        auto mo = setup_map();
        for (auto c : o) {
            mo[get_value(c)]++;
        }

        auto mp = setup_map();
        for (auto c : p) {
            mp[get_value(c)]++;
        }

        print_map(std::cout, mo);
        print_map(std::cout, mp);
        std::cout << solve(0, mo, mp) << '\n';
        print_map(std::cout, mo);
        print_map(std::cout, mp);
    }

    return 0;
}