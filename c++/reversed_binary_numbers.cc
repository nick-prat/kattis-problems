#include <cstdint>
#include <iostream>

int main(int argc, char** argv) {
    std::uint32_t test_index = 0x80000000;
    std::uint32_t test = 0;
    std::uint32_t ans_index = 0x00000001;
    std::uint32_t ans = 0;
    
    std::cin >> test;

    auto copyState = false;
    do {
        if ( ! copyState && test_index & test )
            copyState = true;

        if ( copyState ) {
            if (test_index & test) 
                ans |= ans_index;
            ans_index <<= 1;
        }

        test_index >>= 1;
    } while (test_index != 0);

    std::cout << ans << '\n';
}