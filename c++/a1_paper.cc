#include <sstream>
#include <vector>
#include <iostream>
#include <iterator>
#include <complex>
#include <cmath>

using floating_point = long double;
using dp_state = std::vector<std::vector<floating_point>>;

floating_point get_width(int n, dp_state& dp);
floating_point get_height(int n, dp_state& dp);

floating_point get_height(int n, dp_state& dp) {
    if ( dp[0][n] == 0 )
        dp[0][n] = get_width(n-1, dp);
    return dp[0][n];
}

floating_point get_width(int n, dp_state& dp) {
    if ( dp[1][n] == 0 )
        dp[1][n] = get_height(n-1, dp) / 2;
    return dp[1][n];
}

int find_next(int cursor, std::vector<int> const& S) {
    do {
        cursor--;
    } while ( S[cursor] == 0 && cursor > 0 );
    return cursor;
}

floating_point calculate_tape_length(int cursor, int num, dp_state& dp) {
    floating_point tape = 0;
    while ( num > 1 ) {
        num /= 2;
        tape += get_height(cursor, dp) * num;
        cursor -= 1;
    }
    return tape;
}

template<typename T>
std::ostream& operator<<(std::ostream& stream, std::vector<T> const& vec) {
    stream << '[';
    for ( auto it = std::begin(vec); it != std::end(vec); it++ ) {
        stream << *it;
        if ( std::next(it) != std::end(vec) )
            stream << ',';
    }
    stream << ']';
    return stream;
}

int main(int argc, char** argv) {
    auto dp = std::vector<std::vector<floating_point>>{{}, {}};

    std::cout.precision(18);

    auto line = std::string{};
    getline(std::cin, line);
    auto N = std::stoi(line);

    dp[0].resize(N);
    std::fill(dp[0].begin(), dp[0].end(), 0);
    dp[1].resize(N);
    std::fill(dp[1].begin(), dp[1].end(), 0);

    dp[0][0] = std::pow(2.0l, (-3.0l/4.0l));
    dp[1][0] = std::pow(2.0l, (-5.0l/4.0l));

    getline(std::cin, line);
    auto iss = std::istringstream{line};
    auto S = std::vector<int>{};
    std::copy(std::istream_iterator<int>{iss}, std::istream_iterator<int>{}, std::back_inserter(S));

    get_height(2, dp);

    auto cursor = 1;
    floating_point tape_length = 0;

    while ( S[0] < 2 ) {
        if ( cursor > (N - 2) ) {
            std::cout << "impossible\n";
            return 0;
        }

        if ( S[cursor] >= 2 ) {
            auto wanted_dist = cursor - find_next(cursor, S);
            auto most_dist = static_cast<int>(std::floor(std::log2(S[cursor])));
            auto found_dist = std::min(wanted_dist, most_dist);
            
            auto found_cursor = cursor - found_dist;
            
            tape_length += calculate_tape_length(cursor, std::pow(2, found_dist), dp);

            S[cursor] -= std::pow(2, found_dist);
            S[found_cursor] += 1;
            cursor = found_cursor;
        } else {
            cursor++;
        }
    }

    std::cout << (tape_length + get_height(0, dp)) << '\n';
    return 0;
}