#include <cctype>
#include <cstdint>
#include <iterator>
#include <string>
#include <iostream>
#include <vector>

std::uint8_t to_uint8(char c) {
    return tolower(c) - 97;
}

char to_char(std::uint8_t i) {
    return static_cast<char>(i + 97);
}

int main(int argc, char** argv) {
    auto s = std::string{};
    std::getline(std::cin, s);
    int N = std::stoi(s);

    for (auto i = 0; i < N; i++) {
        std::getline(std::cin, s);

        auto vec = std::vector<int>{};
        for ( std::uint8_t i = 0; i < 26; i++) {
            vec.push_back(0);
        }

        for (auto& c: s) {
            vec[to_uint8(c)]++;
        }

        auto missing = std::string{};
        for ( std::uint8_t i = 0; i < 26; i++) {
            if ( vec[i] == 0 ) {
                missing.push_back(to_char(i));
            }
        }

        if ( missing.empty() ) {
            std::cout << "pangram\n";
        } else {
            std::cout << "missing " << missing << '\n';
        }
    }
}