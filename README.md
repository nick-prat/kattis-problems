# Kattis Problems

### Python
- [A1 Paper](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/a1_paper.py)
- [Akcija](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/akcija.py)
- [Another Brick in the Wall](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/another_brick_in_the_wall.py)
- [A Real Challenge](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/a_real_challenge.py)
- [Beekeeper](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/beekeeper.py)
- [Broken Swords](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/broken_swords.py)
- [Cheating at War](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/cheating_at_war.py)
- [Closest Sums](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/closest_sums.py)
- [Evil Straw Wars Live](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/evil_straw_warts_live.py)
- [Gnoll Hypothesis](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/gnoll_hypothesis.py)
- [Hello World](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/hello_world.py)
- [I Hate the Number Nine](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/i_hate_the_number_nine.py)
- [Knights in Fen](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/knights_in_fen.py)
- [Odd Man Out](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/odd_man_out.py)
- [Polygon Area](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/polygon_area.py)
- [Reversed Binary Numbers](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/reversed_binary_numbers.py)
- [Skener](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/skener.py)
- [Srednji](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/srednji.py)
- [Teacher Evaluation](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/teacher_evaluation.py)
- [Troll Hunt](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/troll_hunt.py)
- [Yoda](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/python/yoda.py)

### Haskell

- [ABC](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/haskell/abc.hs)
- [Above Average](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/haskell/above_average.hs)
- [A List Game](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/haskell/a_list_game.hs)
- [Troll Hunt](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/haskell/troll_hunt.hs)

### C
- [A Different Problem](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/c/a_different_problem.c)

### C++

- [A1 Paper](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/c++/a1_paper.cc)
- [Cheating at War](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/c++/cheating_at_war.cc)
- [Quick Brown Fox](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/c++/quick_brown_fox.cc)
- [Reversed Binary Numbers](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/c++/reversed_binary_numbers.cc)

### Rust

- [Another Brick in the Wall](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/rust/src/another_brick_in_the_wall.rs)
- [Quick Brown Fox](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/rust/src/quick_brown_fox.rs)
- [Right of Way](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/rust/src/right_of_way.rs)
- [Speed Limit](https://gitlab.com/nick-prat/kattis-problems/-/blob/master/rust/src/speed_limit.rs)
