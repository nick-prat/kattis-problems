fn get_int_list() -> Vec<i32> {
    let mut line = String::new();
    return match std::io::stdin().read_line(&mut line) {
        Ok(_) => line.split_whitespace().map(|x| str::parse::<i32>(x).unwrap()).collect::<Vec<i32>>(),
        Err(_) => return Vec::new()
    };
}

fn main() {
    let vals = get_int_list();
    let bs = get_int_list();

    let mut total = 0;
    let mut rows_completed = 0;
    for b in &bs {
        if total < vals[1] {
            total += b;
        }

        if total == vals[1] {
            rows_completed += 1;
            total = 0;
        } else if total > vals[1] {
            println!("NO");
            return;
        }

        if rows_completed == vals[0] {
            println!("YES");
            return;
        }
    }
}