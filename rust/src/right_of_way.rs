fn map_dir(line: &str) -> i32 {
    return match line {
        "North" => 0,
        "East" => 1,
        "South" => 2,
        "West" => 3,
        _ => -1
    }
}

fn get_left(dir: i32) -> i32 {
    return (dir + 1).rem_euclid(4);
}

fn get_right(dir: i32) -> i32 {
    return (dir - 1).rem_euclid(4);
}

fn get_opposite(dir: i32) -> i32 {
    return (dir + 2).rem_euclid(4);
}

fn is_left(dir1: i32, dir2: i32) -> bool {
    return get_left(dir1) == dir2;
}

fn is_right(dir1: i32, dir2: i32) -> bool {
    return get_right(dir1) == dir2;
}

fn is_opposite(dir1: i32, dir2: i32) -> bool {
    return get_opposite(dir1) == dir2;
}

fn main() {
    let mut line = String::new();
    let dirs = match std::io::stdin().read_line(&mut line) {
        Ok(_n) => line.trim().split(" ").map(|x| map_dir(x)).collect::<Vec<_>>(),
        Err(_err) => return
    };

    if is_left(dirs[0], dirs[1]) {
        if is_opposite(dirs[0], dirs[2]) || is_right(dirs[0], dirs[2]) {
            println!("Yes");
            return;
        }
    } else if is_opposite(dirs[0], dirs[1]) {
        if is_right(dirs[0], dirs[2]) {
            println!("Yes");
            return;
        }
    }

    println!("No");
}