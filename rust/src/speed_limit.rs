fn main() {
    let mut line = String::new();

    loop {
        let n = match std::io::stdin().read_line(&mut line) {
            Ok(_n) => match line.trim().parse::<i32>() {
                Ok(num) => num,
                Err(_err) => return
            },
            Err(_err) => return
        };
        line.clear();

        if n == -1 {
            break;
        }
        
        let mut total_dist = 0;
        let mut elapsed_time = 0;
        for _i in 0usize..n as usize {
            match std::io::stdin().read_line(&mut line) {
                Ok(_n) => {
                    let v = line.split_whitespace().collect::<Vec<&str>>();
                    
                    let speed = match v[0].parse::<i32>() {
                        Ok(num) => num,
                        Err(_err) => return
                    };
                    
                    let time = match v[1].parse::<i32>() {
                        Ok(num) => num,
                        Err(_err) => return
                    };

                    total_dist = total_dist + (speed * (time - elapsed_time));
                    elapsed_time = time;
                    line.clear();
                },
                Err(_err) => return
            };
        }

        println!("{} miles", total_dist);
    }
    
}