use std::collections::HashMap;

fn to_lowercase(c: char) -> char {
    return match c.to_lowercase().next() {
        Some(val) => val,
        None => panic!("error parsing input")
    }
}

fn remove_whitespace(s: &mut String) {
    s.retain(|x| ! x.is_whitespace());
}

fn main() {
    let mut line = String::new();
    let n = match std::io::stdin().read_line(&mut line) {
        Ok(_) =>  {
            remove_whitespace(&mut line);
            match str::parse::<i32>(line.as_str()) {
                Ok(val) => val,
                Err(_) => panic!("error parsing input")
            }
        },
        Err(_) => return
    };

    for _ in 0..n {
        let mut m = HashMap::<char, i32>::new();

        line.clear();
        std::io::stdin().read_line(&mut line).unwrap();
    
        line.chars().map(|x| to_lowercase(x)).for_each(|x| { m.insert(x, 1); } );
        
        let mut missing = String::new();
        for a in b'a'..b'z'+1 {
            if !m.contains_key(&(a as char)) {
                missing.push(a as char);
            }
        }

        if missing.is_empty() {
            println!("pangram");
        } else {
            println!("missing {}", missing);
        }
    }
}