import sys, math

N, B = map(int, sys.stdin.readline().strip().split(" "))
A = list(map(int, sys.stdin.readline().strip().split(" ")))

mid = math.inf
for i in range(len(A)):
    if A[i] == B:
        mid = i

left_num_map = {0: 1}
right_num_map = {0: 1}

weight = 0
for i in range(mid -1, -1, -1):
    weight = weight + (1 if A[i] < A[mid] else -1)
    if weight not in left_num_map:
        left_num_map[weight] = 1
    else:
        left_num_map[weight] = left_num_map[weight] + 1

weight = 0
for i in range(mid + 1, len(A), 1):
    weight = weight + (1 if A[i] < A[mid] else -1)
    if weight not in right_num_map:
        right_num_map[weight] = 1
    else:
        right_num_map[weight] = right_num_map[weight] + 1

count = 0
for k in left_num_map:
    ok = k * -1
    if ok in right_num_map:
        count = count + left_num_map[k] * right_num_map[ok]

print(count)
