import sys
from functools import reduce

A = [char for char in sys.stdin.readline().strip()]
B = [char for char in sys.stdin.readline().strip()]

def print_list(A):
    num = list(filter(lambda a : a is not None, A))
    if not num:
        print("YODA")
    else:
        print(int(reduce((lambda a,b : a + b), num)))
    
for i in range(min(len(A), len(B))):
    ai = len(A) - 1 - i
    bi = len(B) - 1 - i

    if A[ai] > B[bi]:
        B[bi] = None
    elif B[bi] > A[ai]:
        A[ai] = None

print_list(A)
print_list(B)