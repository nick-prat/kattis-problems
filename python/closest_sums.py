import math, sys

def process_case(casenum):
    N = sys.stdin.readline().strip()
    if not N:
        return False
    N = int(N)
    I = [int(sys.stdin.readline().strip()) for n in range(N)]
    list.sort(I)
    M = int(sys.stdin.readline().strip())
    C = [int(sys.stdin.readline().strip()) for m in range(M)]
    
    print("Case " + str(casenum) + ":")
    for i in range(len(C)):
        t = C[i]
        if t < I[0]:
            print("Closest sum to " + str(t) + " is " + str(I[0] + I[1]) + ".")
            continue
        
        closest = math.inf
        
        for i in range(len(I)):
            i1 = I[i]
            for i2 in reversed(I[i+1:len(I)]):
                total = i1 + i2
                if abs(total - t) <= abs(closest - t):
                    closest = total
                if total < t or total == t:
                    continue
            if closest == t:
                break

        print("Closest sum to " + str(t) + " is " + str(closest) + ".")
    
    return True

casenum = 1
while(process_case(casenum)):
    casenum = casenum + 1