import sys

H, W, N = [int(x) for x in sys.stdin.readline().strip().split(" ")]
B = [int(x) for x in sys.stdin.readline().strip().split(" ")]

total = 0
rows_completed = 0
for b in B:
    if total < W:
        total += b
    
    if total == W:
        rows_completed += 1
        total = 0
    elif total > W:
        print("NO")
        break

    if rows_completed == H:
        print("YES")
        break