import math, sys

B, K, G = map(int, sys.stdin.readline().strip().split(" "))

print(math.ceil((B - 1) / (math.floor(K / G))))