import math
import sys

while True:
    line = sys.stdin.readline().strip('\n')
    if not line:
        break
    
    M, D, Y, S, E = line.split()
    Sh, Sm = [int(v) for v in S.split(":")]
    Eh, Em = [int(v) for v in E.split(":")]

    mins = (Eh * 60 + Em) - (Sh * 60 + Sm)
    hours = math.floor(mins / 60)
    mins = mins % 60

    print("{} {} {} {} hours {} minutes".format(M, D, Y, hours, mins))