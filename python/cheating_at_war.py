import sys, math

N = int(sys.stdin.readline().strip())

def get_value(letter):
    if letter == "A":
        return 13
    if letter == "T":
        return 9
    if letter == "J":
        return 10
    if letter == "Q":
        return 11
    if letter == "K":
        return 12
    return int(letter) - 1

def setup_dict():
    d = {}
    for i in range(1, 14):
        d[i] = 0
    return d

def get_high(d, start=13):
    for i in range(start, 0, -1):
        if d[i] > 0:
            return i
    return 0

def get_low(d, start=1):
    for i in range(start, 14):
        if d[i] > 0:
            return i
    return 0

def can_beat(d, val):
    for i in range(val+1, 14):
        if d[i] > 0:
            return i
    return 0

def can_tie(d, val):
    return val if d[val] > 0 else 0

def solve(score, o,p):
    for i in range(13, 1, -1):
        while o[i] > 0:
            o[i] = o[i] - 1

            res = can_beat(p, i)
            if res != 0:
                score = score + 2
                p[res] = p[res] - 1
                continue
            
            plow = get_low(p)
            res = can_tie(p, i)
            if res != 0:
                # TODO Recursive solution obviously sucks
                p[res] = p[res] - 1
                tie_score = solve(score + 1, o, p)

                if plow == res:
                    return tie_score

                no = o.copy()
                np = p.copy()

                np[plow] = np[plow] - 1
                loss_score = solve(score, no, np)

                return max(tie_score, loss_score)

            p[plow] = p[plow] - 1
    return score

for i in range(N):
    o = setup_dict()
    for val in sys.stdin.readline().strip():
        num = get_value(val)
        o[num] = o[num] + 1

    p = setup_dict()
    for val in sys.stdin.readline().strip():
        num = get_value(val)
        p[num] = p[num] + 1

    print(solve(0, o,p))