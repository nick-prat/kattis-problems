import sys

N = int(sys.stdin.readline().strip())

def get_n_lines(n):
    count = 0
    while count < n:
        yield int(sys.stdin.readline().strip())
        count += 1

C = sorted(get_n_lines(N), reverse=True)

cost = 0
for i in range(0, N, 3):
    for j in range(i, min(i+2, N)):
        cost += C[j]

print(cost)