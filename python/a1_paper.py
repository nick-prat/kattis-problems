import math, sys

N = int(sys.stdin.readline().strip('\n'))
S = [int(s) for s in sys.stdin.readline().strip('\n').split(" ")]

# ? The largest size of a paper is the length of the uncut side of the size above

A2_HEIGHT = math.pow(2, (-3/4))
A2_WIDTH = math.pow(2, (-5/4))

dp = []
dp.append([0] * N)
dp.append([0] * N)

dp[0][0] = math.pow(2, (-3/4)) # A2 height
dp[1][0] = math.pow(2, (-5/4)) # A2 width

def get_height(i):
    if dp[0][i] != 0:
        return dp[0][i]
    nh = get_width(i-1)
    dp[0][i] = nh
    return nh

def get_width(i):
    if dp[1][i] != 0:
        return dp[1][i]
    nw = get_height(i-1) / 2
    dp[1][i] = nw
    return nw

def find_next(cursor):
    cursor -= 1
    while S[cursor] == 0 and cursor > 0:
        cursor -= 1
    return cursor

def calculate_tape_length(cursor, num):
    tape = 0
    while num > 1:
        num /= 2
        tape += get_height(cursor) * num
        cursor -= 1
    return tape

cursor = 1
tape_length = 0

while S[0] < 2:
    if cursor > (N-2):
        print("impossible")
        quit()

    if S[cursor] >= 2:
        wanted_dist = cursor - find_next(cursor)
        most_dist = math.floor(math.log(S[cursor], 2))
        found_dist = min(wanted_dist, most_dist)

        found_cursor = cursor - found_dist

        tape_length += calculate_tape_length(cursor, math.pow(2, found_dist))

        S[cursor] -= math.pow(2, found_dist)
        S[found_cursor] += 1
        cursor = found_cursor
    else:
        cursor += 1

print(tape_length + get_height(0))