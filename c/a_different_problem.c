#include <stdio.h>
#include <stdlib.h>

typedef long long ll;

int main(int argc, char** argv) {
    ll a = 0, b = 0;
    while ( scanf("%lld %lld", &a, &b) != EOF ) {
        printf("%lld\n", llabs(a - b));
    }
    return 0;
}