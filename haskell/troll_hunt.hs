import Data.List.Split
import Data.Function

foo :: Int -> Int -> Float
foo = (/) `on` fromIntegral

main = do  
    name <- getLine  
    let res = splitOn " " name

    let b = read $ res!!0 :: Int
    let k = read $ res!!1 :: Int
    let g = read $ res!!2 :: Int

    let i = b - 1
    let j = floor $ foo k g

    print $ ceiling $ foo i j