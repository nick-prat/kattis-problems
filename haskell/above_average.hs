import Data.List.Split ( splitOn )
import Text.Printf ( printf )

calc :: [Int] -> Float -> Int
calc [] avg = 0
calc (x:xs) avg
    | (fromIntegral x :: Float) <= avg = calc xs avg
    | otherwise = 1 + calc xs avg

divIntToFloat :: Int -> Int -> Float
divIntToFloat x y = (fromIntegral x :: Float) / (fromIntegral y :: Float)

solve :: Int -> [Int] -> Float
solve count nums = let avg = divIntToFloat (sum nums) count in divIntToFloat (calc nums avg) count

solveCount :: Int -> [Int] -> IO ()
solveCount _ [] = return ()
solveCount n (x:xs)
    | n > 0 = printf "%.3f%%\n" (solve x (take x xs) * 100) >> solveCount (n - 1) (drop x xs)
    | otherwise = return ()

main :: IO ()
main = do
    conts <- getContents
    let nums = map (\x -> read x :: Int) (words conts)
    solveCount (head nums) (tail nums)
