import Data.List.Split
import Data.List (sort)

getVal c
    | c == 'A' = 0
    | c == 'B' = 1
    | c == 'C' = 2
    | otherwise = -1

getSequence :: String -> [Int] -> String
getSequence [x] cs = show (cs !! getVal x)
getSequence (x:xs) cs = show (cs !! getVal x) ++ " " ++ getSequence xs cs

main = do
    numStr <- getLine
    let n = sort . map (\x -> read x :: Int) $ splitOn " " numStr
    seq <- getLine
    putStrLn $ getSequence seq n