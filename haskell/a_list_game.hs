
lowestFactor :: Integral t => t -> t -> t
lowestFactor num start
    | mod num start == 0 = start
    | fromIntegral start < sqrt (fromIntegral num) = lowestFactor num (start + 1)
    | otherwise = num

getLowestFactors :: Integral t => t -> [t]
getLowestFactors num
    | num == lf = [num]
    | otherwise = lf : getLowestFactors (div num lf)
    where lf = lowestFactor num 2

main :: IO ()
main = do
    line <- getLine
    let input = read line :: Int
    print $ length $ getLowestFactors input